<?php

require_once "Roue.php";
require_once "Cylindre.php";
require_once "Moteur.php";
require_once "Pneu.php";
require_once "Porte.php";
require_once "Voiture.php";



$Pneu1 = new Pneu(23,"neige");
$Roue1 = new Roue(4,42.5,$Pneu1);
$Roue2 = new Roue(4,42.5,$Pneu1);
$Roue3 = new Roue(4,42.5,$Pneu1);
$Roue4 = new Roue(4,42.5,$Pneu1);
$cylindre1 = new Cylindre(2,"direct");
$cylindre2 = new Cylindre(2,"direct");
$cylindre3 = new Cylindre(2,"direct");
$cylindre4 = new Cylindre(2,"direct");
$porte1 = new Porte("avant",110);
$porte2 = new Porte("avant",110);
$porte3 = new Porte("arrière",110);
$porte4 = new Porte("arrière",110);
$moteur1 = new Moteur($cylindre1,$cylindre2,$cylindre3,$cylindre4,4000,"manuelle",6);
$Voiture1 = new Voiture($moteur1,$porte1);

