<?php




class Porte{


    private string $type;
    private float $taille;


    public function __construct(string $type, float $taille)
    {
        $this->type = $type;
        $this->taille = $taille;
    }

    /**
     * Get the value of taille
     */ 
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * Set the value of taille
     */ 
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * Get the value of type
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     */ 
    public function setType($type)
    {
        $this->type = $type;
    }
}