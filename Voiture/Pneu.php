<?php


class Pneu
{

    private int $taille;
    private string $type;

    public function __construct(int $taille, string $type)
    {
        $this->taille = $taille;
        $this->type = $type;
    }

    /**
     * Get the value of taille
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * Set the value of taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * Get the value of type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
