<?php


class Cylindre
{
    private int $nbrSoupape;
    private string $injection;

    public function __construct(int $nbrSoupape, string $injection)
    {
        $this->nbrSoupape = $nbrSoupape;
        $this->injection = $injection;
    }
    /**
     * Get the value of nbrSoupape
     */
    public function getNbrSoupape()
    {
        return $this->nbrSoupape;
    }

    /**
     * Set the value of nbrSoupape
     *
     * @return  self
     */
    public function setNbrSoupape($nbrSoupape)
    {
        $this->nbrSoupape = $nbrSoupape;

        return $this;
    }

    /**
     * Get the value of injection
     */
    public function getInjection()
    {
        return $this->injection;
    }

    /**
     * Set the value of injection
     *
     * @return  self
     */
    public function setInjection($injection)
    {
        $this->injection = $injection;

        return $this;
    }
}
