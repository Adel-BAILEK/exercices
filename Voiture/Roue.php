<?php



class Roue
{
    private int $ecrou;
    private float $diametre;
    private Pneu $pneu;

    public function __construct(int $ecrou, float $diametre , Pneu $pneu)
    {
        $this->ecrou = $ecrou;
        $this->diametre = $diametre;
        $this->pneu = $pneu;
    }

    /**
     * Get the value of pneu
     */ 
    public function getPneu()
    {
        return $this->pneu;
    }

    /**
     * Set the value of pneu
     */ 
    public function setPneu($taille , $type)
    {
        $this->pneu = new Pneu($taille , $type);
    }

    /**
     * Get the value of diametre
     */ 
    public function getDiametre()
    {
        return $this->diametre;
    }

    /**
     * Set the value of diametre
     */ 
    public function setDiametre($diametre)
    {
        $this->diametre = $diametre;
    }

    /**
     * Get the value of ecrou
     */ 
    public function getEcrou()
    {
        return $this->ecrou;
    }

    /**
     * Set the value of ecrou
     */ 
    public function setEcrou($ecrou)
    {
        $this->ecrou = $ecrou;
    }
}
