<?php


class Voiture
{

    private Moteur $moteur;
    private Porte $porte1;
    private Porte $porte2;
    private Porte $porte3;
    private Porte $porte4;
    private Roue $roue1;
    private Roue $roue2;
    private Roue $roue3;
    private Roue $roue4;


    public function __construct(Moteur $moteur = null, Porte $porte = null, Roue $roue)
    {
        $this->moteur = $moteur;
        $this->porte = $porte;
        $this->roue = $roue;
    }

    /**
     * Get the value of moteur
     */
    public function getMoteur()
    {
        return $this->moteur;
    }

    /**
     * Set the value of moteur
     */
    public function setMoteur(
        Cylindre $cylindre1,
        $Cylindre2,
        $Cylindre3,
        $Cylindre4,
        float $puissance,
        string $transmission,
        int $nbrVitesse
    ) {
        $this->moteur = new Moteur(
            $cylindre1,
            $Cylindre2,
            $Cylindre3,
            $Cylindre4,
            $puissance,
            $transmission,
            $nbrVitesse
        );
    }

    /**
     * Get the value of porte
     */
    public function getPorte()
    {
        return $this->porte;
    }

    /**
     * Set the value of porte
     */
    public function setPorte($type, $taille)
    {
        $this->porte = new Porte($type, $taille);
    }

    /**
     * Get the value of roue
     */
    public function getRoue()
    {
        return $this->roue;
    }

    /**
     * Set the value of roue
     */
    public function setRoue($ecrou, $diametre, $pneu)
    {
        $this->roue = new Roue($ecrou, $diametre, $pneu);
    }
}
