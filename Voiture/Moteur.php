<?php



class Moteur
{

    private Cylindre $cylindre1;
    private Cylindre $cylindre2;
    private Cylindre $cylindre3;
    private Cylindre $cylindre4;
    private float $puissance;
    private string $tansmission;
    private int $nbrVitesse;



    public function __construct(
        Cylindre $cylindre1,
        Cylindre $cylindre2,
        Cylindre $cylindre3,
        Cylindre $cylindre4,
        float $puissance,
        string $transmission,
        int $nbrVitesse
    ) {
        $this->cylindre = $cylindre1;
        $this->cylindre = $cylindre2;
        $this->cylindre = $cylindre3;
        $this->cylindre = $cylindre4;
        $this->puissance = $puissance;
        $this->transmission = $transmission;
        $this->nbrVitesse = $nbrVitesse;
    }

    /**
     * Get the value of nbrVitesse
     */
    public function getNbrVitesse()
    {
        return $this->nbrVitesse;
    }

    /**
     * Set the value of nbrVitesse
     */
    public function setNbrVitesse($nbrVitesse)
    {
        $this->nbrVitesse = $nbrVitesse;
    }

    /**
     * Get the value of tansmission
     */
    public function getTansmission()
    {
        return $this->tansmission;
    }

    /**
     * Set the value of tansmission
     */
    public function setTansmission($tansmission)
    {
        $this->tansmission = $tansmission;
    }

    /**
     * Get the value of puissance
     */
    public function getPuissance()
    {
        return $this->puissance;
    }

    /**
     * Set the value of puissance
     */
    public function setPuissance($puissance)
    {
        $this->puissance = $puissance;
    }

    /**
     * Get the value of cylindre
     */
    public function getCylindre1()
    {
        return $this->cylindre;
    }

    /**
     * Set the value of cylindre
     */
    public function setCylindre1($nbrSoupape, $injection)
    {
        $this->cylindre1 = new Cylindre($nbrSoupape, $injection);
    }
    /**
     * Get the value of cylindre
     */
    public function getCylindre2()
    {
        return $this->cylindre;
    }

    /**
     * Set the value of cylindre
     */
    public function setCylindre2($nbrSoupape, $injection)
    {
        $this->cylindre2 = new Cylindre($nbrSoupape, $injection);
    }
    /**
     * Get the value of cylindre
     */
    public function getCylindre3()
    {
        return $this->cylindre;
    }

    /**
     * Set the value of cylindre
     */
    public function setCylindre3($nbrSoupape, $injection)
    {
        $this->cylindre3 = new Cylindre($nbrSoupape, $injection);
    }
    /**
     * Get the value of cylindre
     */
    public function getCylindre4()
    {
        return $this->cylindre;
    }

    /**
     * Set the value of cylindre
     */
    public function setCylindre4($nbrSoupape, $injection)
    {
        $this->cylindre4 = new Cylindre($nbrSoupape, $injection);
    }
}
